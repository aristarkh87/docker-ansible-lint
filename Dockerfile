FROM python:3.6-alpine

RUN apk add --no-cache --virtual .build-deps \
        build-base \
        libffi-dev \
        libressl-dev \
    && pip install ansible-lint \
    && apk del --purge .build-deps

WORKDIR /playbook

CMD ["ansible-lint", "--version"]
